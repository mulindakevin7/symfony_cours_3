<?php

namespace App\Controller;

use App\Entity\Commentaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;


class MainController extends AbstractController
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    #[Route('/', name: 'app_main')]
    public function index(): Response
    {
        $articles =$this->entityManager->getRepository(Article::class)->findAll();
        return $this->render('main/index.html.twig', [
            'controller_name' => 'page Blog',
            'articles' => $articles,
        ]);
    }
    #[Route('article/{id}', name: 'article_show')]
    public function single(Article $article):Response
    {
        $commentaires = $this->entityManager->getRepository(Commentaire::class)->findBy(['article'=>$article]);
        return $this->render('main/single.html.twig', [
            'article' => $article,
            'commentaires' => $commentaires
        ]);

    }

}

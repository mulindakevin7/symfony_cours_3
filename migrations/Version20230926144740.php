<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230926144740 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE commentaire (id INT AUTO_INCREMENT NOT NULL, article_id INT DEFAULT NULL, message VARCHAR(255) NOT NULL, INDEX IDX_67F068BC7294869C (article_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC7294869C FOREIGN KEY (article_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E66EC5D4C30');
        $this->addSql('DROP INDEX IDX_23A0E66EC5D4C30 ON article');
        $this->addSql('ALTER TABLE article ADD description LONGTEXT NOT NULL, DROP categorie_article_id');
        $this->addSql('ALTER TABLE categorie DROP FOREIGN KEY FK_497DD634EC5D4C30');
        $this->addSql('DROP INDEX IDX_497DD634EC5D4C30 ON categorie');
        $this->addSql('ALTER TABLE categorie DROP categorie_article_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC7294869C');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('ALTER TABLE article ADD categorie_article_id INT DEFAULT NULL, DROP description');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E66EC5D4C30 FOREIGN KEY (categorie_article_id) REFERENCES categorie (id)');
        $this->addSql('CREATE INDEX IDX_23A0E66EC5D4C30 ON article (categorie_article_id)');
        $this->addSql('ALTER TABLE categorie ADD categorie_article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE categorie ADD CONSTRAINT FK_497DD634EC5D4C30 FOREIGN KEY (categorie_article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_497DD634EC5D4C30 ON categorie (categorie_article_id)');
    }
}
